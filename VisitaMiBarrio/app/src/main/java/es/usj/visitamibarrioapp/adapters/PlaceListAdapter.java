package es.usj.visitamibarrioapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import es.usj.visitamibarrioapp.R;
import es.usj.visitamibarrioapp.models.Place;

public class PlaceListAdapter extends ArrayAdapter<Place> implements Filterable {

    private List<Place> places;
    private List<Place> filteredPlaces;
    private PlaceFilter placeFilter;
    private LayoutInflater inflater;

    public PlaceListAdapter(Context context, int resource, ArrayList<Place> places) {
        super(context, resource, places);
        this.places = places;
        this.filteredPlaces= places;
        getFilter();
    }

    @Override
    public int getCount() {
        return filteredPlaces.size();
    }

    @Override
    public Place getItem(int i) {
        return filteredPlaces.get(i);
    }

    @Override
    public void clear() {
        this.filteredPlaces.clear();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        Place place = (Place) getItem(position);

        if (convertView == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_item, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            // get view holder back
            holder = (ViewHolder) convertView.getTag();
        }

        holder.photo = convertView.findViewById(R.id.place_image);
        Glide.with(convertView).load(place.getMainPhoto()).into(holder.photo);
        holder.name = convertView.findViewById(R.id.place_name);
        holder.name.setText(place.getName());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (placeFilter == null) {
            placeFilter = new PlaceFilter();
        }

        return placeFilter;
    }

    static class ViewHolder {
        TextView name;
        ImageView photo;
    }

    private class PlaceFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<Place> tempList = new ArrayList<Place>();

                // search content in friend list
                for (Place place : places) {
                    if (place.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(place);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = places.size();
                filterResults.values = places;
            }

            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredPlaces = (ArrayList<Place>) results.values;
            setNotifyOnChange(true);
            notifyDataSetChanged();
        }
    }


}