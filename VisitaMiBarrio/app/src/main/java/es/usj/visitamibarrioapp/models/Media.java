package es.usj.visitamibarrioapp.models;

import org.parceler.Parcel;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

@Parcel
public class Media implements Serializable {
    private String mUrl;
    private String mTitle;
    private String mType;
    private String mdate;

    // empty constructor needed by the Parceler library
    public Media(){

    }

    public Media(String url, String title, String type) {
        mUrl = url;
        mTitle = title;
        mType = type;
        mdate = DateFormat.getDateTimeInstance().format(new Date());
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setType(String Type) {
        mType = Type;
    }

    public String getType(){
        return mType;
    }

    public String getDate() { return mdate; }

}
