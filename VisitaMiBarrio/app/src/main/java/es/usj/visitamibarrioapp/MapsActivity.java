package es.usj.visitamibarrioapp;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


import es.usj.usuario.bottomsheet.GoogleMapsBottomSheetBehavior;
import es.usj.visitamibarrioapp.helpers.PolylineJsonParser;
import es.usj.visitamibarrioapp.models.Place;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, StreetViewPanorama.OnStreetViewPanoramaChangeListener {

    private static final int REQUEST_CODE_PHONE = 2;
    private PlacesListFragment placesListFragment;
    private SupportStreetViewPanoramaFragment streetViewPanoramaFragment;
    private StreetViewPanorama mStreetViewPanorama;

    private ArrayList<Place> places = new ArrayList<Place>();
    private GoogleMap mGoogleMap;
    public static String PLACESELECTED = "PLACESELECTED";
    private Boolean IsMapView = false;
    private SupportMapFragment mapFragment;
    private PlacesListFragment placeFragment;
    private GoogleMapsBottomSheetBehavior behavior;
    private View parallax;
    private Place currentPlace;
    LocationManager mLocationManager;
    Location actualLocal;
    boolean isPollylineVisible = false;

    Toolbar toolbar,searchtollbar;
    Menu search_menu;
    MenuItem item_search;
    private SearchView searchView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSearchtollbar();
        toolbar.bringToFront();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        places = (ArrayList<Place>) getIntent().getSerializableExtra(SplashActivity.PLACESDATA);

        //Init Bottom detail screen
        initBottomSheet();

        // add PhoneStateListener
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        //Create list fragment and pass array as parameter
        placesListFragment = new PlacesListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PLACES", places);
        placesListFragment.setArguments(bundle);

    }

    public void setPlaceFromFragment(Place placeFilteredSelected){
        currentPlace = placeFilteredSelected;
        if (currentPlace != null)
            setPlaceOnBottomSeet(currentPlace);
            behavior.setState(GoogleMapsBottomSheetBehavior.STATE_EXPANDED);

        if (searchView.isShown()) {
            item_search.collapseActionView();
            searchView.setQuery("", false);
        }
    }

    public void setPlaceOnBottomSeet(Place elementSelected) {
        currentPlace = elementSelected;
        behavior.setState(GoogleMapsBottomSheetBehavior.STATE_COLLAPSED);
        behavior.setHideable(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(currentPlace.getPosition()));

        streetViewPanoramaFragment = (SupportStreetViewPanoramaFragment) getSupportFragmentManager().findFragmentById(R.id.parallax);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(new OnStreetViewPanoramaReadyCallback() {
            @Override
            public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                mStreetViewPanorama = panorama;
                mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(MapsActivity.this);
                // Only need to set the position once as the streetview fragment will maintain
                // its state.
                mStreetViewPanorama.setPosition(currentPlace.getPosition());
            }
        });

        fillContextViews();
    }

    private void updateFragmentContent(String option) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (option == "map") {
            transaction.replace(R.id.map, placesListFragment);
        } else {
            transaction.replace(R.id.map, mapFragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void initBottomSheet() {
        final View bottomsheet = findViewById(R.id.bottomsheet);
        behavior = GoogleMapsBottomSheetBehavior.from(bottomsheet);
        parallax = findViewById(R.id.parallax);
        behavior.setParallax(parallax);
        behavior.setPeekHeight(200);
        behavior.setAnchorHeight(450);

        // wait for the bottomsheet to be laid out
        bottomsheet.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // set the height of the parallax to fill the gap between the anchor and the top of the screen
                CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(parallax.getMeasuredWidth(), behavior.getAnchorOffset() / 2);
                parallax.setLayoutParams(layoutParams);
                bottomsheet.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        behavior.setBottomSheetCallback(new GoogleMapsBottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, @GoogleMapsBottomSheetBehavior.State int newState) {
                // each time the bottomsheet changes position, animate the camera to keep the pin in view
                // normally this would be a little more complex (getting the pin location and such),
                // but for the purpose of an example this is enough to show how to stay centered on a pin
                if (currentPlace != null)
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentPlace.getLatitude(), currentPlace.getLongitude()), 14.0f));
                else
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(places.get(0).getLatitude(), places.get(0).getLongitude()), 14.0f));
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (isPollylineVisible) {
            menu.add(Menu.NONE, R.string.action_clean, Menu.NONE, R.string.action_clean).setIcon(R.drawable.clean)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        if (IsMapView){
            menu.add(Menu.NONE, R.string.action_search, Menu.NONE, R.string.action_search).setIcon(R.drawable.ic_search)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.add(Menu.NONE,  R.string.action_list, Menu.NONE, R.string.action_list).setIcon(R.drawable.map)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } else {
            menu.add(Menu.NONE,  R.string.action_list, Menu.NONE, R.string.action_list).setIcon(R.drawable.list)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.string.action_search:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    circleReveal(R.id.searchtoolbar,1,true,true);
                }
                else searchtollbar.setVisibility(View.VISIBLE);

                searchtollbar.bringToFront();
                item_search.expandActionView();
                return true;

            case R.string.action_list:
                if (!IsMapView) {
                    updateFragmentContent("map");
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.map));


                } else {
                    updateFragmentContent("list");
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.list));
                }

                invalidateOptionsMenu();
                IsMapView = !IsMapView;
                mapFragment.onResume();
                return true;
            case R.string.action_clean:
                isPollylineVisible= false;
                invalidateOptionsMenu();
                loadMarkers();

                TextView tvDistance = (TextView) findViewById(R.id.distance);
                tvDistance.setText("");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.mGoogleMap = map;
        behavior.anchorMap(map);

        if (places.size() > 0)
            loadMarkers();

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Place elementSelected = (Place) marker.getTag();
                if (elementSelected != null) {
                    setPlaceOnBottomSeet(elementSelected);
                    return true;
                }
                return false;
            }
        });

        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                behavior.setHideable(true);
                behavior.setState(GoogleMapsBottomSheetBehavior.STATE_HIDDEN);
            }
        });

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Request location permissions
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        }

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(places.get(0).getLatitude(), places.get(0).getLongitude()), 14.0f));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {

                    Toast.makeText(this, "No se pudo optener permiso", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    void loadMarkers() {
        // Creating a marker
        mGoogleMap.clear();
        for (int i = 0; i < places.size(); i++) {
            final Place myPlace = places.get(i);

            final MarkerOptions marker = new MarkerOptions()
                    .position(new LatLng(myPlace.getLatitude(), myPlace.getLongitude()))
                    .title(myPlace.getName());

            Glide.with(getApplicationContext()).asBitmap().load(myPlace.getMainPhoto())
                    .into(new SimpleTarget<Bitmap>(50, 50) {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(resource);
                            marker.icon(icon);
                            Marker markerResult = mGoogleMap.addMarker(marker);
                            markerResult.setTag(myPlace);
                        }
                    });
        }

        //Apply Zoom
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(places.get(0).getLatitude(), places.get(0).getLongitude()), 14.0f));
    }

    public void fillContextViews() {
        TextView tvPlaceName = (TextView) findViewById(R.id.placeName);
        TextView tvPlaceAddress = (TextView) findViewById(R.id.placeAddress);

        TextView tvPlaceNameContent = (TextView) findViewById(R.id.placeName1);
        TextView tvPlaceAddressContent = (TextView) findViewById(R.id.placeAddress1);

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar2);

        TextView tvPhone = (TextView) findViewById(R.id.phone);
        ImageView ivMainPhoto = (ImageView) findViewById(R.id.mainPhoto);
        ImageButton ibWebSite = (ImageButton) findViewById(R.id.buttonWebSite);
        ImageButton ibGallery = (ImageButton) findViewById(R.id.buttonGallery);
        ImageButton ibRoute = (ImageButton) findViewById(R.id.buttonGo);
        ImageButton ibPhone = (ImageButton) findViewById(R.id.phoneCall);

        Glide.with(this).load(currentPlace.getMainPhoto())
                .thumbnail(0.5f)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(ivMainPhoto);

        if (Objects.equals(currentPlace.getRating(), "-NA-"))
            ratingBar.setRating(Float.parseFloat("0.0"));
        else ratingBar.setRating(Float.parseFloat(currentPlace.getRating()));
        tvPhone.setText(currentPlace.getPhone());
        tvPlaceName.setText(currentPlace.getName());
        tvPlaceAddress.setText(currentPlace.getAddress());

        tvPlaceNameContent.setText(currentPlace.getName());
        tvPlaceAddressContent.setText(currentPlace.getAddress());

        ibGallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(MapsActivity.this, GalleryActivity.class);
                intent.putExtra(MapsActivity.PLACESELECTED, Parcels.wrap(currentPlace));
                startActivity(intent);
            }
        });

        ibRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Getting URL to the Google Directions API

                List<String> providers = mLocationManager.getProviders(true);
                if (ActivityCompat.checkSelfPermission(
                        MapsActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                    //Get user location
                    Location location= getLastKnownLocation();
                    if (location== null) {
                        Toast.makeText(MapsActivity.this, getResources().getString(R.string.error_getting_user_location), Toast.LENGTH_LONG).show();
                        return;
                    }
                    String url = getUrl(new LatLng(location.getLatitude(), location.getLongitude()), new LatLng(currentPlace.getLatitude(), currentPlace.getLongitude())).toString();
                    Log.d("onMapClick", url.toString());
                    PolylineTask FetchUrl = new PolylineTask();

                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                }
            });

        ibWebSite.setOnClickListener(new View.OnClickListener()

            {
                public void onClick (View v){
                Intent intent = new Intent(MapsActivity.this, WebSiteActivity.class);
                intent.putExtra("url", currentPlace.getWebSite());
                startActivity(intent);
            }
            });


        ibPhone.setOnClickListener(new View.OnClickListener()

            {
                public void onClick (View v){
                Intent intent = new Intent(Intent.ACTION_DIAL);
                /// /intent.setData(Uri.parse(uri));
                intent.setData(Uri.parse("tel:" + currentPlace.getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent, REQUEST_CODE_PHONE);
            }
            });

        }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public StringBuilder getUrl(LatLng origin, LatLng dest) {

        String apiKey = getResources().getString(R.string.google_direction_key);
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        sb.append("origin=" + origin.latitude + "," + origin.longitude);
        sb.append("&destination=" + dest.latitude + "," + dest.longitude);
        sb.append("&sensor=false");
        sb.append("&mode=driving");
        sb.append("&key=");
        sb.append(apiKey);
        return sb;
    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
        mStreetViewPanorama.setPosition(streetViewPanoramaLocation.position);
    }

    public void setSearchtollbar() {

        searchtollbar = (Toolbar) findViewById(R.id.searchtoolbar);
        searchtollbar.bringToFront();
        if (searchtollbar != null) {
            searchtollbar.inflateMenu(R.menu.menu_search);
            search_menu=searchtollbar.getMenu();

            searchtollbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        circleReveal(R.id.searchtoolbar,1,true,false);
                    else
                        searchtollbar.setVisibility(View.GONE);
                }
            });

            item_search = search_menu.findItem(R.id.action_filter_search);

            MenuItemCompat.setOnActionExpandListener(item_search, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    // Do something when collapsed
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.searchtoolbar,1,true,false);
                    }
                    else
                        searchtollbar.setVisibility(View.GONE);
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    // Do something when expanded
                    return true;
                }
            });

            initSearchView();

        } else
            Log.d("toolbar", "setSearchtollbar: NULL");
    }

    public void initSearchView() {
        searchView = (SearchView) search_menu.findItem(R.id.action_filter_search).getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        // Enable/Disable Submit button in the keyboard
        searchView.setSubmitButtonEnabled(false);

        // Change search close button image
        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.ic_close);

        // set hint and the text colors
        EditText txtSearch = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        txtSearch.setHint("Search..");
        txtSearch.setHintTextColor(Color.DKGRAY);
        txtSearch.setTextColor(getResources().getColor(R.color.colorPrimary));

        // set the cursor
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }

            public void callSearch(String query) {
                placesListFragment.plAdapter.getFilter().filter(query);
                placesListFragment.setListAdapter(placesListFragment.plAdapter);
                placesListFragment.plAdapter.notifyDataSetChanged();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow) {

        final View myView = findViewById(viewID);
        int width=myView.getWidth();

        if(posFromRight>0)
            width-=(posFromRight*getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        if(containsOverflow)
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);

        int cx=width;
        int cy=myView.getHeight()/2;

        Animator anim;
        if(isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0,(float)width);
        else anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float)width, 0);

        anim.setDuration((long)220);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();
    }

    //monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        private boolean isPhoneCalling = false;
        String LOG_TAG = "LOGGING 123";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }
    }

    //Get Polylines
    private class PolylineTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadPolyline(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserPolylineTask parserTask = new ParserPolylineTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }

        private String downloadPolyline(String strUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url
                urlConnection.connect();

                // Reading data from url
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb = new StringBuffer();

                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                data = sb.toString();
                Log.d("downloadUrl", data.toString());
                br.close();

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }

    }

    /**
     * A class to parse the Google Places in JSON format
     */
    public class ParserPolylineTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PolylineJsonParser parser = new PolylineJsonParser();
                routes = parser.parse(jObject);

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            String distanceO= "NA";
            String durationO = "NA";
            if(result != null){

                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(0);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");


                List<HashMap<String, String>> pathR = result.get(1);
                HashMap<String, String> m = pathR.get(0);

                distanceO = m.get("distance");
                durationO = m.get("duration");
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                graphPolyline(lineOptions, distanceO, durationO);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }

        void graphPolyline(PolylineOptions lineOptions, String distance, String durationO) {
            mGoogleMap.clear();
            List<LatLng> points = lineOptions.getPoints();
            MarkerOptions origin = new MarkerOptions();
            origin.position(points.get(0));
            origin.title("Me");
            origin.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
            mGoogleMap.addMarker(origin);
            MarkerOptions destination = new MarkerOptions();
            destination.position(points.get(points.size() - 1));
            destination.title(currentPlace.getName());
            destination.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
            mGoogleMap.addMarker(destination);
            mGoogleMap.addPolyline(lineOptions);

            TextView tvDistance = (TextView) findViewById(R.id.distance);
            tvDistance.setText(distance + " - " + durationO);

            behavior.setState(GoogleMapsBottomSheetBehavior.STATE_COLLAPSED);

            //move map camera
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(currentPlace.getLatitude() , currentPlace.getLongitude())));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(14));

            isPollylineVisible= true;
            invalidateOptionsMenu();

        }
    }

}
