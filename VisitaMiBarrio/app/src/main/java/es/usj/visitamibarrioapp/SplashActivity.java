package es.usj.visitamibarrioapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import es.usj.visitamibarrioapp.helpers.PlaceDetailsJSONParser;
import es.usj.visitamibarrioapp.helpers.PlaceJSONParser;
import es.usj.visitamibarrioapp.models.Place;

public class SplashActivity extends AppCompatActivity {

    public static String PLACESDATA = "Data";
    private ArrayList<Place> places = new ArrayList<Place>();
    int listCount = 0;
    int listTotal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);

        //Get places from api
        StringBuilder sbValue = new StringBuilder(sbMethod());
        PlacesTask getPlacesTask = new PlacesTask();
        getPlacesTask.execute(sbValue.toString());
    }

    /** A class, to download Google Places */
    private class PlacesTask extends AsyncTask<String, Integer, String> {
        String data = null;

        @Override
        protected String doInBackground(String... url) {
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }

    public StringBuilder sbMethod() {

        String apiKey = getResources().getString(R.string.google_places_key);
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + getResources().getString(R.string.neighboard_latitud) + "," +getResources().getString(R.string.neighboard_longitud));
        sb.append("&radius=8000");
        sb.append("&sensor=true");
        sb.append("&key=");
        sb.append(apiKey);
        Log.d("Map", "api: " + sb.toString());
        return sb;
    }

    /** A method to download json data from url */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Toast.makeText(this, getResources().getString(R.string.internet_connection_error), Toast.LENGTH_LONG).show();

            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){

            if(list == null) {
               return;
            }

            listTotal= 10;
            for(int i=0;i<listTotal;i++){
                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);
                String reference = hmPlace.get("reference");
                String apiKey = getResources().getString(R.string.google_places_key);

                StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
                sb.append("reference="+reference);
                sb.append("&sensor=true");
                sb.append("&key=");
                sb.append(apiKey);
                // Creating a new non-ui thread task to download Google place details
                PlacesDetailTask getPlacesDetailTask = new PlacesDetailTask();
                // Invokes the "doInBackground()" method of the class PlaceTask
                getPlacesDetailTask.execute(sb.toString());
            }
        }

    }

    /** A class, to download Google Place Details */
    private class PlacesDetailTask extends AsyncTask<String, Integer, String>{

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserDetailTask parserTask = new ParserDetailTask();

            // Start parsing the Google place details in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Place Details in JSON format */
    private class ParserDetailTask extends AsyncTask<String, Integer, HashMap<String,String>>{

        JSONObject jObject;
        Place myPlace;
        String photo;
        // Invoked by execute() method of this object
        @Override
        protected HashMap<String,String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Start parsing Google place details in JSON format
                hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }

            String name = hPlaceDetails.get("name");
            String icon = hPlaceDetails.get("icon");
            String vicinity = hPlaceDetails.get("vicinity");
            String lat = hPlaceDetails.get("lat");
            String lng = hPlaceDetails.get("lng");
            String formatted_address = hPlaceDetails.get("formatted_address");
            String formatted_phone = hPlaceDetails.get("formatted_phone");
            String website = hPlaceDetails.get("website");
            String rating = hPlaceDetails.get("rating");
            String international_phone_number = hPlaceDetails.get("international_phone_number");
            String latitude = hPlaceDetails.get("latitude");
            String longitude = hPlaceDetails.get("longitude");
            photo = hPlaceDetails.get("photo");
            myPlace= new Place(name, Double.parseDouble(latitude), Double.parseDouble(longitude), photo, website, formatted_address, formatted_phone, rating);

            return hPlaceDetails;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(HashMap<String,String> hPlaceDetails){

            places.add(myPlace);
            listCount++;

            if(listCount == listTotal){
                Intent intent = new Intent();
                intent.setClass(SplashActivity.this, MapsActivity.class);
                intent.putExtra(PLACESDATA, places);
                startActivity(intent);
                finish();
            }

        }
    }
}