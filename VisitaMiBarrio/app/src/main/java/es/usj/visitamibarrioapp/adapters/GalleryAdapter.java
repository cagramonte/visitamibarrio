package es.usj.visitamibarrioapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.util.List;

import es.usj.visitamibarrioapp.R;
import es.usj.visitamibarrioapp.models.Media;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private List<Media> media;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnailImage;
        public ImageButton thumbnailIcon;

        public MyViewHolder(View view) {
            super(view);
            thumbnailImage = (ImageView) view.findViewById(R.id.thumbnailImage);
            thumbnailIcon = (ImageButton) view.findViewById(R.id.imageButtonVideo);
        }
    }


    public GalleryAdapter(Context context, List<Media> media) {
        mContext = context;
        this.media = media;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_thumbnail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Media itemMedia = media.get(position);
        String path = mContext.getExternalFilesDir(null) + "/" + itemMedia.getUrl();
        File file = new File(path);

        if (itemMedia.getType() == "video")
            holder.thumbnailIcon.setVisibility(View.VISIBLE);
        else holder.thumbnailIcon.setVisibility(View.INVISIBLE);

        if (file.exists()){
            Glide.with(mContext)
                    .load(mContext.getExternalFilesDir(null) + "/" + itemMedia.getUrl())
                    .thumbnail(0.5f)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(holder.thumbnailImage);
        }
        holder.thumbnailIcon.bringToFront();
    }

    @Override
    public int getItemCount() {
        return media.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private GalleryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final GalleryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
