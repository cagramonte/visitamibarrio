package es.usj.visitamibarrioapp.models;

import com.google.android.gms.maps.model.LatLng;
import org.parceler.Parcel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Parcel
public class Place  implements Serializable{
    private String name;
    private String description;
    private String mainPhoto;
    private String webSite;
    private String address;
    private String phone;
    private static List<Media> allMedia = new ArrayList<Media>();
    private double latitude;
    private double longitude;
    private String rating;

    // empty constructor needed by the Parceler library
    public Place(){

    }

    public Place(String _name, double _latitud, double _longitude, String _photo, String _webSite, String _address, String _phone, String _rating) {
        name = _name;
        mainPhoto = _photo;
        webSite= _webSite;
        latitude = _latitud;
        longitude = _longitude;
        address = _address;
        phone= _phone;
        rating = _rating;
    }
    public String getRating(){
        return rating;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){ return description;}

    public String getWebSite(){ return webSite;}

    public String getAddress(){ return address;}

    public String getPhone(){ return phone;}

    public LatLng getPosition(){
        return new LatLng(latitude, longitude);
    }

    public String getMainPhoto(){
        return mainPhoto;
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){ return longitude;}

    public List<Media> getAllMedia(){
        return allMedia;
    }

    public void addMedia(Media media){
        allMedia.add(media);
    }

}
