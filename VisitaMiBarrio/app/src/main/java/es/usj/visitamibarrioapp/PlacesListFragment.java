package es.usj.visitamibarrioapp;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import es.usj.visitamibarrioapp.adapters.PlaceListAdapter;
import es.usj.visitamibarrioapp.models.Place;

import java.util.ArrayList;
import java.util.List;


public class PlacesListFragment  extends ListFragment implements AdapterView.OnItemClickListener {

    private ArrayList<Place> places = new ArrayList<Place>();
    PlaceListAdapter plAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        places = (ArrayList<Place>)getArguments().getSerializable("PLACES");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        plAdapter= new PlaceListAdapter(this.getContext(), R.layout.fragment_item, places);
        setListAdapter(plAdapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        ((MapsActivity)getActivity()).setPlaceFromFragment(plAdapter.getItem(position));
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}