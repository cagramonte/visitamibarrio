package es.usj.visitamibarrioapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.usj.visitamibarrioapp.adapters.GalleryAdapter;
import es.usj.visitamibarrioapp.models.Media;
import es.usj.visitamibarrioapp.models.Place;

public class GalleryActivity extends AppCompatActivity {

    private List<Media> arrayMedia = new ArrayList<Media>();
    public static String MEDIASELECT = "MediaSelected";
    private Place place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        place = (Place) Parcels.unwrap(getIntent().getParcelableExtra(MapsActivity.PLACESELECTED));

        if (place.getAllMedia()  != null)  {
            arrayMedia = place.getAllMedia();
        }

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutManager);

        GalleryAdapter mAdapter = new GalleryAdapter(getApplicationContext(), arrayMedia);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("medias", (Serializable) arrayMedia);
                bundle.putInt("position", position);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        setTitle(R.string.gallery);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options_gallery, menu);
        return true;
    }

    @Override
    public void onResume() {
        if (arrayMedia != null && arrayMedia.size() != 0){
            int index = arrayMedia.size() - 1;
            String filePath = getExternalFilesDir(null) + "/" + arrayMedia.get(index).getUrl();
            File file = new File(filePath);
            if(!file.exists()){
                arrayMedia.remove(index);
            }
        }
        super.onResume();
        this.onRestart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String fileName;
        Intent intent;
        switch (item.getItemId()) {
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.take_video_item:
                requestCamaraPermission("video");
                return true;
            case R.id.take_photo_item:
                requestCamaraPermission("camera");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void openVideo(){
        String fileName;
        Intent intent;
        intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileName = place.getName()+ "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File video = new File(getExternalFilesDir(null), fileName);

        Uri videoURI = FileProvider.getUriForFile(
                this,
                this.getApplicationContext().getPackageName() + ".provider",
                video);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI);

        place.addMedia(new Media(fileName, place.getName(), "video"));
        startActivity(intent);

    }

    void openCamara(){
        String fileName;
        Intent intent;
        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileName = place.getName()+ "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File foto = new File(getExternalFilesDir(null), fileName);

        Uri photoURI = FileProvider.getUriForFile(
                this,
                this.getApplicationContext().getPackageName() + ".provider",
                foto);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        place.addMedia(new Media(fileName, place.getName(),"image"));
        startActivity(intent);

    }
    void requestCamaraPermission(String type){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            if(type=="camera")
                openCamara();
            else
                openVideo();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, type=="camera"? 1:2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamara();
                } else {

                    Toast.makeText(this, "No se pudo optener permiso", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openVideo();
                } else {

                    Toast.makeText(this, "No se pudo optener permiso", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
